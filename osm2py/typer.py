class Typer:
    types: dict = None

    def __init__(self):
        if not self.types:
            self.load()

    @classmethod
    def load(cls):
        from .user import User
        from . import Node, Way, Relation
        from .changeset import Changeset

        cls.types = {
            'node': Node,
            'way': Way,
            'relation': Relation,
            'changeset': Changeset,
            'user': User
        }

    def __getitem__(self, t: str):
        return self.types[t]
