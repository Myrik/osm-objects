from collections import OrderedDict
from logging import getLogger
from typing import List

import requests

from .changeset import Changeset
from .libs.dict_to_xml import deserialize
from .libs.loader import Loader, Status
from .libs.tags import Tags
from .libs.url import OSMUrl

logger = getLogger()


class POI(Loader):
    t: str
    id: int
    version: int

    changeset: Changeset = None
    tags: Tags = None

    def __init__(self, osm_id: int, version=-1):
        if type(self) == POI:
            raise NotImplementedError()
        self.id = int(osm_id)
        self.version = version

    @classmethod
    def from_url(cls, url: str):
        obj = OSMUrl(url).obj

        if cls not in (POI, type(obj)):
            logger.warning(f'from_url return another type: {cls} -> {type(obj)}')

        return obj

    def __str__(self):
        text = self.t[0] + str(self.id)
        if self.version != -1:
            text += f'v{self.version}'
        return text

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        if isinstance(other, POI):
            return other.t == self.t and other.version == self.version and other.id == self.id
        return False

    def __hash__(self):
        return hash(str(self))

    def __int__(self):
        return int(self.id)

    def load(self, item=None) -> dict:
        if type(self) is POI:
            raise NotImplementedError()

        if not item:
            url = OSMUrl.from_obj(self, api=True)
            resp = requests.get(url)

            if resp.status_code == 404:
                raise Exception(f'{self.t.capitalize()} with id={self.id} does not exists')

            xml = deserialize(resp.text)
            item = xml['osm'][self.t]

        self.id = int(item['id'])
        self.version = int(item['version'])
        self.changeset = Changeset(int(item['changeset']))
        self.tags = Tags(item.get('tag', []))
        self.status = Status.loaded

        return item

    @staticmethod
    def load_multi(objects: List['POI']) -> List:
        items = {
            'node': OrderedDict(),
            'way': OrderedDict(),
            'relation': OrderedDict(),
        }
        for obj in objects:
            items[obj.t].update({obj.id: obj})

        for t, ids in items.items():
            if ids:
                if len(ids) > 1:
                    url = OSMUrl.multi(t, [k for k, v in ids.items() if v.status != Status.loaded])
                    resp = requests.get(str(url))
                    data = deserialize(resp.text)
                    parsed_items = data['osm'][t]
                    for i in parsed_items:
                        items[t][int(i['id'])].load(i)
                elif len(ids) == 1:
                    list(ids.values())[0].load()

        return [items[obj.t][obj.id] for obj in objects]
