import re
from datetime import datetime

from requests import Session

from .libs.dict_to_xml import deserialize
from .libs.haversine import TLatLon
from .libs.loader import Loader


class User(Loader):
    _api = f'https://api.openstreetmap.org/api/0.6'
    t = 'user'
    id: int
    name: str
    created: datetime
    roles = ()
    home: TLatLon = None

    changesets: int
    traces: int

    login: str
    password: str
    session: Session = None
    __csrf = None

    def __init__(self, osm_id: int):
        self.id = osm_id
        self.session = Session()

    def load(self):
        if self.session and self.id == -1:
            resp = self.session.get(f'{self._api}/user/details').text
        else:
            resp = self.session.get(f'{self._api}/user/{self.id}').text

        data = deserialize(resp)
        payload = data['osm'][self.t]

        self.id = int(payload['id'])
        self.name = payload['display_name']
        self.created = datetime.strptime(payload['account_created'], "%Y-%m-%dT%H:%M:%SZ")

        roles = payload.get('roles')
        if roles:
            if isinstance(roles, dict):
                self.roles = tuple(roles.keys())
            else:
                self.roles = (roles,)

        self.changesets = int(payload['changesets']['count'])
        self.traces = int(payload['traces']['count'])
        if 'home' in payload:
            home = payload['home']
            self.home = (float(home['lat']), float(home['lon']))

    def api_req(self, url: str, **kwargs):
        kwargs.update({
            'utf8': '✓',
            'authenticity_token': self.csrf
        })
        return self.session.post('https://www.openstreetmap.org/' + url, kwargs)

    @property
    def csrf(self) -> str:
        if self.__csrf:
            return self.__csrf
        page = self.session.get('https://www.openstreetmap.org/').text
        items = re.findall(r'<meta.*name="csrf-token".*content="(.*?)".*/>', page)
        self.__csrf = items[0]
        return self.__csrf

    @classmethod
    def auth(cls, login, password) -> 'User':
        obj = cls(-1)
        obj.session.auth = (login, password)
        kwargs = {
            'utf8': '✓',
            'authenticity_token': obj.csrf,
            'username': login,
            'password': password,
            'commit': 'Представиться'
        }
        rez = obj.session.post('https://www.openstreetmap.org/login', kwargs)
        msgs = re.findall('<div class="message">(.*?)</div>', rez.text)
        if msgs:
            raise Exception(msgs)
        if "<span class='username'>" not in rez.text:
            raise Exception('Unknown error')

        return obj

    def __str__(self):
        return f'u{self.id}'
