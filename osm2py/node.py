from shapely.geometry import Point

from .libs.cached_property import CachedProperty
from .poi import POI
from .libs.haversine import TLatLon


class Node(POI):
    t = 'node'
    latlon: TLatLon

    @CachedProperty
    def geo(self) -> Point:
        return Point(self.latlon[::-1])

    def load(self, item=None) -> 'Node':
        item = super().load(item)

        self.latlon = float(item['lat']), float(item['lon'])

        return self
