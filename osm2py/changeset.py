from datetime import datetime
from typing import Tuple

import requests

from .typer import Typer
from .libs.dict_to_xml import serialize, deserialize
from .libs.haversine import TLatLon
from .libs.loader import Loader
from .libs.meta import __tool_name__
from .libs.tags import Tags
from .libs.url import OSMUrl
from .user import User


class Changeset(Loader):
    t = 'changeset'
    id = -1

    text = ''
    bot = True
    visible = True
    can_delete = False

    created_at: datetime
    closed_at: datetime
    user: User
    box: Tuple[TLatLon, TLatLon]
    open: bool
    tags: Tags

    def __init__(self, osm_id: int):
        self.id = osm_id

        self.create = []
        self.modify = []
        self.delete = []

    @classmethod
    def create_new(cls, text='', bot=True, visible=True, can_delete=False):
        obj = cls(-1)

        obj.text = text
        obj.bot = bot
        obj.visible = visible
        obj.can_delete = can_delete

    def load(self):
        url = OSMUrl.from_obj(self, api=True)
        resp = requests.get(url).text
        data = deserialize(resp)

        itm = data['osm'][self.t]

        dt_format = "%Y-%m-%dT%H:%M:%SZ"
        self.created_at = datetime.strptime(itm['created_at'], dt_format)
        self.closed_at = datetime.strptime(itm['closed_at'], dt_format)
        self.open = itm['open'] == 'true'
        self.box = (
            (float(itm['min_lat']), float(itm['min_lon'])),
            (float(itm['max_lat']), float(itm['max_lon']))
        )
        self.tags = Tags(itm.get('tag', []))

        self.download()

    def download(self):
        url = OSMUrl.from_obj(self, api=True) + '/download'
        resp = requests.get(url).text
        data = deserialize(resp)

        self.create = []
        self.modify = []
        self.delete = []

        payload = data['osmChange']
        typer = Typer()
        for item in payload.get('create', []):
            t = list(item.keys())[0]
            item_data = item[t]
            self.create.append(typer[t](int(item_data['id']), int(item_data['version'])))
        for item in payload.get('modify', []):
            t = list(item.keys())[0]
            item_data = item[t]
            self.modify.append(typer[t](int(item_data['id']), int(item_data['version'])))
        for item in payload.get('delete', []):
            t = list(item.keys())[0]
            item_data = item[t]
            self.delete.append(typer[t](int(item_data['id']), int(item_data['version'])))

        return self

    def doc(self) -> str:
        data = {
            'osmChange': {
                'version': '0.6',
                'generator': __tool_name__,

                'create': self.create,
                'modify': self.modify,
                'delete': {'if-unused': 'true' if self.can_delete else 'false'}
            }
        }

        if self.delete:
            data['osmChange']['delete'].update(self.delete)

        return serialize(data, pretty_print=False)

    def meta(self) -> str:
        tags = {
            'created_by': __tool_name__,
            'comment': self.text,
            'bot': 'yes' if self.bot else 'no'
        }

        data = {
            'osm': {
                'version': '0.6',
                'generator': __tool_name__,

                'changeset': {
                    'visible': 'true' if self.visible else 'false',
                    'tag': [
                        {'k': k, 'v': v} for k, v in tags.items()
                    ]
                }
            }
        }

        return serialize(data)

    def __str__(self):
        return f'c{self.id}'

    def __int__(self):
        return self.id

    def __eq__(self, other):
        return isinstance(other, Changeset) and other.id == self.id
