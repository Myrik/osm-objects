from typing import List, Union

from shapely.geometry import LineString, Polygon

from .libs.cached_property import CachedProperty
from .poi import POI
from .node import Node


class Way(POI):
    t = 'way'
    nodes: List[Node]

    @CachedProperty
    def geo(self) -> Union[Polygon, LineString]:
        if self.nodes[0] == self.nodes[-1]:
            self.load_full()
            return Polygon([i.latlon[::-1] for i in self.nodes])
        else:
            return LineString([i.latlon[::-1] for i in self.nodes])

    def load(self, item=None) -> 'Way':
        item = super().load(item)

        nds = item.get('nd', [])
        if isinstance(nds, dict):
            nds = [nds]

        self.nodes = [Node(nd['ref']) for nd in nds]

        return self

    def load_full(self):
        self.nodes = self.load_multi(self.nodes)
        return self

    def __contains__(self, item: Node):
        return self.geo.contains(item.geo)
