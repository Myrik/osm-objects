from typing import Dict

from .poi import POI
from .typer import Typer


class Relation(POI):
    t = 'relation'
    members: Dict[POI, str]

    def load(self, item=None) -> 'Relation':
        item = super().load(item)

        members = item.get('member', [])
        if isinstance(members, dict):
            members = [members]

        self.members = {}
        for member in members:
            obj = Typer()[member['type']](member['ref'])
            self.members.update({obj: member['role']})

        return self

    def load_full(self):
        keys, values = zip(*self.members.items())
        keys = self.load_multi(keys)
        self.members = dict(zip(keys, values))
        return self
