import os

import pytest
import requests_mock

from osm2py import Way, User, Changeset, Node, Relation, POI
from osm2py.libs.loader import Status
from osm2py.libs.tags import Tags
from osm2py.libs.url import OSMUrl


@pytest.fixture(scope='function')
def mocker():
    data_dir = os.path.dirname(__file__) + '/tests_data/'
    'https://api.openstreetmap.org/api/0.6/ways?ways='
    r59199_ways = (
        29122548, 29079916, 29079892, 50638253, 572768150, 626199681,
        615512518, 615642795, 50638255, 26788497, 31148963, 572768135, 572768134
    )
    w66094056_nodes = (802241469, 802241398, 802241889, 802241486, 6208360289)

    def serialize(mas):
        return ','.join(str(i) for i in mas)

    get = {
        'user_details.xml': 'https://api.openstreetmap.org/api/0.6/user/details',
        'index.xml': 'https://www.openstreetmap.org/',
        'user_3560.xml': 'https://api.openstreetmap.org/api/0.6/user/3560',
        'c65789993.xml': 'https://api.openstreetmap.org/api/0.6/changeset/65789993',
        'c65789993_download.xml': 'https://api.openstreetmap.org/api/0.6/changeset/65789993/download',
        'n2769346988+n4124581489.xml': 'https://api.openstreetmap.org/api/0.6/nodes?nodes=2769346988,4124581489',
        'r59199.xml': 'https://api.openstreetmap.org/api/0.6/relation/59199',
        'r59199_ways.xml': 'https://api.openstreetmap.org/api/0.6/ways?ways=' + serialize(r59199_ways),
        'w66094056.xml': 'https://api.openstreetmap.org/api/0.6/way/66094056',
        'w66094056_nodes.xml': 'https://api.openstreetmap.org/api/0.6/nodes?nodes=' + serialize(w66094056_nodes)
    }

    load_nodes = (4124581489, 2769346988)
    for node in load_nodes:
        get.update({
            f'n{node}.xml': f'https://api.openstreetmap.org/api/0.6/node/{node}'
        })

    post = {
        'login.xml': 'https://www.openstreetmap.org/login',
    }

    with requests_mock.Mocker() as m:
        for k, url in get.items():
            file_path = data_dir + k
            with open(file_path) as f:
                m.get(url, text=f.read())

        for k, url in post.items():
            file_path = data_dir + k
            with open(file_path) as f:
                m.post(url, text=f.read())

        yield m


class TestUser:
    def test_user(self, mocker):
        obj = User(3560)
        assert obj.name == 'Firefishy'
        obj.session.close()

    def test_user_auth(self, mocker):
        obj = User.auth('any@mail.ru', '123456')
        assert obj.name == 'MyrikLD'
        obj.session.close()


class TestTags:
    def test_tags(self):
        tags = Tags([
            {'k': 'tag', 'v': '1'},
            {'k': 'tag', 'v': '2'},
        ])
        assert tags.dict == {'tag': '1; 2'}


class TestUrlParser:
    def test_parse(self):
        url = 'https://api.openstreetmap.org/api/0.6/way/66094056'
        ourl = OSMUrl(url)
        assert url == str(ourl)
        assert ourl.t_obj == Way
        assert ourl.id == 66094056

    def test_multi(self):
        url = 'https://api.openstreetmap.org/api/0.6/ways?ways=66094056,66094057'
        ourl = OSMUrl(url)
        assert url == str(ourl)
        assert ourl.t_obj == Way
        assert ourl.ids == [66094056, 66094057]


class TestChangeset:
    def test_changeset(self, mocker):
        obj = Changeset(65789993)
        assert Node(6164200167, 1) in obj.create
        assert obj.status == Status.loaded


class TestPOI:
    def test_from_url(self, mocker):
        r59199 = POI.from_url('https://api.openstreetmap.org/api/0.6/relation/59199')
        assert r59199 == Relation(59199)
        n2769346988 = POI.from_url('https://api.openstreetmap.org/api/0.6/node/2769346988')
        assert n2769346988 == Node(2769346988)
        c65789993 = POI.from_url('https://api.openstreetmap.org/api/0.6/changeset/65789993')
        assert c65789993 == Changeset(65789993)
        w66094056 = POI.from_url('https://api.openstreetmap.org/api/0.6/way/66094056')
        assert w66094056 == Way(66094056)

    def test_node(self, mocker):
        obj = Node(4124581489)
        assert obj.status == Status.not_loaded
        assert obj.latlon == (53.883921, 27.5434187)
        assert obj.status == Status.loaded

    def test_way(self, mocker):
        obj = Way(66094056)
        changeset = obj.changeset
        assert int(changeset) == 66309201
        assert obj.status == Status.loaded

    def test_relation(self, mocker):
        obj = Relation(59199)
        assert obj.status == Status.not_loaded
        changeset = obj.changeset
        assert int(changeset) == 62663625
        assert obj.status == Status.loaded
        obj.load_full()

    def test_relation_load_full(self, mocker):
        obj = Relation(59199)
        members = list(obj.members)
        assert all(i.status == Status.not_loaded for i in members)
        obj.load_full()
        assert all(i.status == Status.loaded for i in members)

    def test_multi(self, mocker):
        clean_items = [Node(2769346988), Node(4124581489), Relation(59199)]
        objects = POI.load_multi(clean_items)
        assert len(objects) == 3
        assert all(int(objects[i]) == clean_items[i].id for i in range(3))
        assert all(objects[i].status == Status.loaded for i in range(3))

    def test_full(self, mocker):
        obj = Way(66094056)
        assert [int(i) for i in obj.nodes[:-1]] == [802241469, 802241398, 802241889, 802241486, 6208360289]
        assert all(i.status == Status.not_loaded for i in obj.nodes)
        obj.load_full()
        assert all(i.status == Status.loaded for i in obj.nodes)


class TestGEO:
    def test_way(self, mocker):
        obj = Way(66094056)
        obj.geo

    def test_node(self, mocker):
        obj = Node(4124581489)
        obj.geo
