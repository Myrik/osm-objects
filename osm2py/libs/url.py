import re
from typing import List, Iterable

from ..typer import Typer


class OSMUrl:
    api = False
    t_obj = None
    id: int
    ids: List[int]

    _api_re = re.compile(
        r'(?:https?://)?api\.openstreetmap\.org/api/0\.6/'
        r'(?P<t>node|way|relation|changeset)?/(?P<id>\d+)'
    )
    _api_multi_re = re.compile(
        r'(?:https?://)?api\.openstreetmap\.org/api/0\.6/'
        r'(?P<t>node|way|relation|changeset)s\?(?P<t1>node|way|relation|changeset)s=(?P<ids>[\d,]+)'
    )
    _site_re = re.compile(
        r'(?:https?://)?www\.openstreetmap\.org/(?P<t>node|way|relation|changeset)/(?P<id>\d+)'
    )

    def __init__(self, url: str):
        typer = Typer()

        rez = self._api_multi_re.search(url)
        if rez:
            rez = rez.groupdict()
            if rez['t'] != rez['t1']:
                raise Exception(f'Invalid url: {url}')
            self.api = True
            self.t_obj = typer[rez['t']]
            self.ids = [int(i) for i in rez['ids'].split(',')]
            return

        rez = self._api_re.search(url)
        if rez:
            rez = rez.groupdict()
            self.api = True
            self.t_obj = typer[rez['t']]
            self.id = int(rez['id'])
            return

        rez = self._site_re.search(url)
        if rez:
            rez = rez.groupdict()
            self.api = False
            self.t_obj = typer[rez['t']]
            self.id = int(rez['id'])
            return

        raise Exception(f'Unknown url: {url}')

    @property
    def obj(self):
        return self.t_obj(self.id)

    @property
    def objects(self):
        return [self.t_obj(i) for i in self.ids]

    @classmethod
    def from_obj(cls, obj, api=False):
        url = f'https://www.openstreetmap.org/{obj.t}/{obj.id}'

        if obj.t != 'changeset' and obj.version != -1:
            url += f'/{obj.version}'

        ourl = cls(url)
        ourl.api = api
        return str(ourl)

    @classmethod
    def multi(cls, t: str, ids: Iterable):
        items = ','.join([str(i) for i in ids])
        return cls(f'https://api.openstreetmap.org/api/0.6/{t}s?{t}s={items}')

    def __str__(self):
        t = self.t_obj.t

        if hasattr(self, 'id'):
            if self.api:
                return f'https://api.openstreetmap.org/api/0.6/{t}/{self.id}'
            else:
                return f'https://www.openstreetmap.org/0.6/{t}/{self.id}'
        else:
            if not self.api:
                raise Exception('Multiple ids has no web url')
            ids = ','.join([str(i) for i in self.ids])
            return f'https://api.openstreetmap.org/api/0.6/{t}s?{t}s={ids}'
