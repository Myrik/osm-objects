from logging import getLogger

logger = getLogger(__name__)


class Status:
    not_loaded = 0
    loading = 1
    loaded = 2


class Loader:
    status = Status.not_loaded

    def __getattribute__(self, name):
        status = object.__getattribute__(self, 'status')

        if name == 'load' and status is Status.loaded:
            return lambda x: None

        if status is Status.not_loaded and name not in ('t', 'id', 'version', 'status', 'load', 'session'):
            object.__setattr__(self, 'status', Status.loading)
            logger.debug(f'Loading: {self}')
            object.__getattribute__(self, 'load')()
            object.__setattr__(self, 'status', Status.loaded)

        return object.__getattribute__(self, name)
