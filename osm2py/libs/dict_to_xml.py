"""
Dict to XML serializer.
The identity API prefers attributes over elements, so we serialize that way
by convention, and TODO(dolph): configure exceptions.
"""

import re
from collections import defaultdict

from lxml import etree

# illustrating an easy * complex test cases, respectively
DOCTYPE = '<?xml version="1.0" encoding="UTF-8"?>'


def serialize(d, pretty_print=True):
    """Serialize a dictionary to XML"""
    assert len(d.keys()) == 1, 'Cannot encode more than one root element'

    # name the root dom element
    name = list(d.keys())[0]

    # only the root dom element gets an xlmns, TODO(dolph): ... I think?
    root = etree.Element(name)

    try:
        populate_element(root, d[name])
    except Exception as e:
        raise Exception(f'Error in data: {d}\n{e.__class__.__name__}: {e}')

    return '%s\n%s' % (DOCTYPE, etree.tounicode(root, pretty_print=pretty_print))


def populate_element(element, d):
    """Populates an etree with the given dictionary"""
    for k, v in d.items():
        if type(v) is dict:
            # serialize the child dictionary
            child = etree.Element(k)
            populate_element(child, v)
            element.append(child)
        elif type(v) in (list, tuple):
            # serialize the child list
            if k[-1] == 's':
                name = k[:-1]
            else:
                name = k
            if v:
                for item in v:
                    child = etree.Element(name)
                    populate_element(child, item)
                    element.append(child)
            else:
                element.append(etree.Element(name))
        else:
            # add attributes to the current element
            element.set(k, str(v))


def deserialize(text: str):
    if not text:
        return None

    return xml_to_dict(
        etree.fromstring(
            text.encode('utf-8')
        )
    )


def xml_to_dict(t):
    tag = re.sub('{.*?}', '', t.tag)
    d = {tag: {} if t.attrib else None}
    children = list(t)
    if children:
        dd = defaultdict(list)
        for dc in map(xml_to_dict, children):
            for k, v in dc.items():
                dd[k].append(v)
        d = {tag: {k: v[0] if len(v) == 1 else v for k, v in dd.items()}}
    if t.attrib:
        d[tag].update((k, v) for k, v in t.attrib.items())
    if t.text:
        text = t.text.strip()
        if children or t.attrib:
            if text:
                d[tag]['#text'] = text
        else:
            d[tag] = text
    return d
