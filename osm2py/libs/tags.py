from typing import Dict, Set


class Tags:
    _data: Dict[str, Set[str]] = {}

    def __init__(self, items):
        if isinstance(items, dict):
            items = [items]

        self._data = {}
        for item in items:
            k = str(item['k']).strip()
            v = str(item['v']).strip()
            if ';' in v:
                v = {i.strip() for i in v.split(';')}
            else:
                v = {v}

            if k in self._data:
                self._data[k].update(v)
            else:
                self._data[k] = v

    @property
    def dict(self):
        data = {}
        for k, v in self._data.items():
            data[k] = '; '.join(sorted(v))
        return data

    def __repr__(self):
        return str(self.dict)
