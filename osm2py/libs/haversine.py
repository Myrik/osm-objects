from math import asin, cos, radians, sin, sqrt
from typing import Tuple

TLatLon = Tuple[float, float]


def haversine(a: TLatLon, b: TLatLon) -> float:
    lat1, lon1, lat2, lon2 = map(radians, a + b)

    a = sin((lat2 - lat1) / 2) ** 2 + cos(lat1) * cos(lat2) * sin((lon2 - lon1) / 2) ** 2
    c = 2 * asin(sqrt(a))
    r = 6371  # Radius of earth in kilometers. Use 3956 for miles
    return c * r
