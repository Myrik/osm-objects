import re

import setuptools

with open('README.md') as fh:
    long_description = fh.read()

with open('Pipfile') as f:
    text = f.read()

    block = re.findall(r'\[packages\](.*?)\[', text, re.DOTALL)
    items = re.findall(r'(\w+) ?= ?"(.*)"', block[0])
    install_requires = [i[0] + i[1] for i in items]

    block = re.findall(r'\[dev-packages\](.*?)\[', text, re.DOTALL)
    items = re.findall(r'(\w+) ?= ?"(.*)"', block[0])
    tests_require = [i[0] + i[1] for i in items]

setuptools.setup(
    name='OSM2PY',
    version='0.0.1',
    packages=setuptools.find_packages(),
    license='MIT',
    author='myrik',
    author_email='myrik260138@tut.by',
    description='OSM integration for python',
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=install_requires,
    tests_require=tests_require
)
